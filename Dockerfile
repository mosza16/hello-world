# fetch docker image from https://hub.docker.com/_/node 
FROM node:10.16.0-alpine

# set maintainer name
LABEL maintainer "Author name of docker file"

# create directory /app
RUN mkdir /app

# set "/app" is a work directory 
WORKDIR /app

# copy package.json to work directory
COPY package.json .

# install packages
RUN npm install

# copy all files in project directory to work directory
COPY . .

# run npm build script in package.json
RUN npm run build

# run remove src directory 
RUN rm -rf src

# start application server 
CMD ["npm", "start"]

