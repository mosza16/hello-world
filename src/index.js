import '@babel/polyfill'
import Koa from 'koa'
import Router from 'koa-router'
import { createContext } from 'vm';

const app = new Koa()
const router = new Router()

router.get("/", async  ctx => ctx.body = "Hello")

const port = process.env.PORT || "3000"

app.use(router.routes()).use(router.allowedMethods())
app.listen(port)

console.log(`Server is running at http://localhost:${port}`)